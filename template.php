<?php

/**
 * Reference Modules
 *
 * @param $javascript
 */
// https://www.drupal.org/sandbox/mukeysh/2461261
//
// Bootstrap Material Design:
// https://www.drupal.org/sandbox/othermachines/2535992
/**
 * @file
 * Template overrides as well as (pre-)process and alter hooks for the
 * Omega Materialize theme.
 */
function omega_materialize_preprocess_webform_form(&$element, &$component) {
  $submitted = &$element['form']['submitted'];
  $action = &$element['form']['actions'];

  foreach ($submitted as $index => &$item) {
    if (is_array($item) && isset($item['#type'])) {
      $item['#prefix'] = '<div class="row">';
      $item['#suffix'] = '</div>';
      $item['#wrapper_attributes']['class'] = array(
        'col',
        's6'
      );
      switch ($item['#type']) {
        case 'textfield':
        case 'webform_email':
          $item['#attributes']['class'][] = 'validate';
          $item['#wrapper_attributes']['class'][] = 'input-field';
          break;
        case 'textarea':
          $item['#attributes']['class'][] = 'materialize-textarea';
          $item['#wrapper_attributes']['class'][] = 'input-field';
          break;
        case 'radios';
          // To create a radio button with a gap, add class="with-gap". Otherwise it will be solid.
          foreach ($item as $key => $radio) {
            if (is_array($radio) && isset($radio['#type'])) {
              $item[$key]['#attributes']['class'][] = 'with-gap';
            }
          }
          break;
        default:
          break;
      }
    }
  }

  foreach ($action as $index => &$item) {
    if (is_array($item) && isset($item['#type'])) {
      $item['#attributes']['class'] = array(
        'btn',
        'waves-effect',
        'waves-light',
      );
    }
  }
}

/**
 * Add Class to the Label tag.
 *
 * @param $variables
 *
 * @return string
 * @throws \Exception
 */
function omega_materialize_form_element_label(&$variables) {
  $element = $variables['element'];
  // This is also used in the installer, pre-database setup.
  $t = get_t();

  // If title and required marker are both empty, output no label.
  if ((!isset($element['#title']) || $element['#title'] === '') && empty($element['#required'])) {
    return '';
  }

  // If the element is required, a required marker is appended to the label.
  $required = !empty($element['#required']) ? theme('form_required_marker', array('element' => $element)) : '';

  $title = filter_xss_admin($element['#title']);

  $attributes = array();
  // Style the label as class option to display inline with the element.
  if ($element['#title_display'] == 'after') {
    $attributes['class'] = 'option';
  }
  // Show label only to screen readers to avoid disruption in visual flows.
  elseif ($element['#title_display'] == 'invisible') {
    $attributes['class'] = 'element-invisible';
  }

  if (!empty($element['#id'])) {
    $attributes['for'] = $element['#id'];
  }

  // The leading whitespace helps visually separate fields from inline labels.
  return ' <label class="active" ' . drupal_attributes($attributes) . '>' . $t('!title !required', array(
    '!title' => $title,
    '!required' => $required
  )) . "</label>\n";
}


/**
 * Returns HTML for a textarea form element.
 *
 * @param $variables
 *   An associative array containing:
 *   - element: An associative array containing the properties of the element.
 *     Properties used: #title, #value, #description, #rows, #cols, #required,
 *     #attributes
 *
 * @ingroup themeable
 */
function omega_materialize_textarea($variables) {
  $element = $variables['element'];
  element_set_attributes($element, array('id', 'name', 'cols', 'rows'));
  _form_set_class($element, array('form-textarea'));

  $wrapper_attributes = array(
    'class' => array('form-textarea-wrapper'),
  );

  // Add resizable behavior.
  if (!empty($element['#resizable'])) {
    drupal_add_library('system', 'drupal.textarea');
    $wrapper_attributes['class'][] = 'resizable';
  }

  // Remove the textarea't wrapper calss. This action will also get rid of the "resizable" function.
  $output = '<textarea' . drupal_attributes($element['#attributes']) . '>' . check_plain($element['#value']) . '</textarea>';

  return $output;
}

/**
 * User function function theme_form_element
 *
 * @param $variables
 */
function omega_materialize_form_alter(&$form, &$form_state, $form_id) {
  foreach ($form as $index => $item) {
    if (is_array($item) && isset($item['#type'])) {
      $element = &$form[$index];
      switch ($item['#type']) {
        case 'submit':
          $element['#attributes']['class'] = isset($element['#attributes']['class'])
            ? $element['#attributes']['class'] : array(
              'btn',
              'waves-effect',
              'waves-light'
            );
          break;
        case 'actions':
          foreach ($element as $action => $attrs) {
            is_array($attrs) && isset($attrs['#type'])
              ? $element[$action]['#attributes']['class'] = array(
              'btn',
              'waves-effect',
              'waves-light'
            )
              : NULL;
          }
          break;
        case 'container':
          if (isset($element['und'])) {
            foreach ($element['und'] as $key => $value) {
              if (is_array($value) && isset($value['#type'])) {
                if ($value['#type'] == 'text_format') {
                  $element['und'][$key]['#attributes']['class'][] = 'materialize-textarea';
                }
                // @TODO
                if ($value['#type'] == 'managed_file') {
                  //
                }
              }

              // Single Value
              elseif (isset($element['und']['#type'])) {
                if ($value && $value === 'select') {
                  // $element['#attributes']['class'][] = 'input-field';
                }
                if ($value && $value === 'text_format') {
                  // $element['#attributes']['class'][] = 'materialize-textarea';
                }
              }

              // @TODO
              // text_format
              // fieldset
            }
          }
          break;
        case 'textfield':
        case 'password':
        case 'select':
        default:
          break;
      }
    }
  }
}

function omega_materialize_form_element(&$variables) {
  $element = &$variables['element'];

  $classes = array(
    'col',
    's6'
  );

  switch ($element['#type']) {
    case 'textfield':
      $element['#attributes'] = array('class' => $classes);

      break;
    default:
      break;
  }

  return theme_form_element($variables);
}

/**
 * Theme primary tabs
 *
 * @param $variables
 *
 * @return string
 */
function omega_materialize_menu_local_tasks(&$variables) {
  $output = '';

  if (!empty($variables['primary'])) {
    $variables['primary']['#prefix'] = '<nav><div class="nav-wrapper">';
    $variables['primary']['#prefix'] .= '<h2 class="element-invisible">' . t('Primary tabs') . '</h2>';
    $variables['primary']['#prefix'] .= '<ul class="left">';
    $variables['primary']['#suffix'] = '</ul></div></nav>';
    $output .= drupal_render($variables['primary']);
  }
  if (!empty($variables['secondary'])) {
    $variables['secondary']['#prefix'] = '<nav><div class="nav-wrapper">';
    $variables['secondary']['#prefix'] .= '<h2 class="element-invisible">' . t('Secondary tabs') . '</h2>';
    $variables['secondary']['#prefix'] .= '<ul class="right">';
    $variables['secondary']['#suffix'] = '</ul></div></nav>';
    $output .= drupal_render($variables['secondary']);
  }

  return $output;
}

/**
 * Implements theme_menu_local_tasks().
 */
//function omega_materialize_menu_local_tasks(&$variables) {
//  $primary = isset($variables['primary']) ? $variables['primary'] : NULL;
//  $secondary = isset($variables['secondary']) ? $variables['secondary'] : NULL;
//  $output = '';
//  if (!empty($variables['primary'])) {
//    $variables['primary']['#prefix'] = '<h2 class="element-invisible">' . t('Primary tabs') . '</h2>';
//    $variables['primary']['#prefix'] .= '<ul class="tabs z-depth-1">';
//    $variables['primary']['#suffix'] = '</ul>';
//    $output .= drupal_render($variables['primary']);
//  }
//  if (!empty($variables['secondary'])) {
//    $variables['secondary']['#prefix'] = '<h2 class="element-invisible">' . t('Secondary tabs') . '</h2>';
//    $variables['secondary']['#prefix'] .= '<ul class="tabs z-depth-1">';
//    $variables['secondary']['#suffix'] = '</ul>';
//    $output .= drupal_render($variables['secondary']);
//  }
//  return $output;
//}


/**
 * Checks if there is library with specified file.
 */
function check_library($library_name, $file_name) {
  if (!module_exists('libraries')) {
    drupal_set_message(t('materialize requires the Libraries API module, please install it from https://drupal.org/project/libraries before using the theme.'), 'error', FALSE);
    return FALSE;
  }

  $path = libraries_get_path($library_name);
  if ($path && is_file($path . '/' . $file_name)) {
    return TRUE;
  }
  return FALSE;
}

/////////////////  Menu Themes   ///////////////////
/**
 * materialize theme wrapper function for the primary menu links.
 */
function omega_materialize_menu_tree__primary(&$variables) {
  return '<ul class="menu">' . $variables['tree'] . '</ul>';
}

/**
 * Implements theme_menu_tree().
 */
function omega_materialize_menu_tree(&$variables) {
  return '<div class="collection with-header">' . $variables['tree'] . '</div>';
}

/**
 * materialize theme wrapper function for the secondary menu links.
 */
function omega_materialize_menu_tree__secondary(&$variables) {
  return '<ul class="menu">' . $variables['tree'] . '</ul>';
}

/**
 * Implements theme_menu_tree().
 */
function omega_materialize_menu_tree__shortcut_set(&$variables) {
  return theme_menu_tree($variables);
}

/**
 * Semanticui theme wrapper function for the submenu links.
 */
function omega_materialize_menu_tree__submenu(&$variables) {
  return '<div class="menu">' . $variables['tree'] . '</div>';
}

/**
 * Implements theme_menu_local_tasks().
 */
function omega_materialize_menu_local_task($variables) {
  $link = $variables ['element']['#link'];
  $link_text = $link ['title'];

  if (!empty($variables ['element']['#active'])) {
    // Add text to indicate active tab for non-visual users.
    $active = '<span class="element-invisible">' . t('(active tab)') . '</span>';

    // If the link does not contain HTML already, check_plain() it now.
    // After we set 'html'=TRUE the link will not be sanitized by l().
    if (empty($link ['localized_options']['html'])) {
      $link ['title'] = check_plain($link ['title']);
    }
    $link ['localized_options']['html'] = TRUE;
    $link_text = t('!local-task-title!active', array('!local-task-title' => $link ['title'], '!active' => $active));
  }

  return '<li' . (!empty($variables ['element']['#active']) ? ' class="active tab"' : ' class="tab"') . '>' . l($link_text, $link ['href'], $link ['localized_options']) . "</li>\n";
}

/**
 * Overrides theme_menu_link().
 */
function omega_materialize_menu_link(array $variables) {
  $element = $variables['element'];
  $sub_menu = '';
  $element['#localized_options']['attributes']['class'][] = 'collection-item';
  if ($element['#below']) {
    $element['#below']['#theme_wrappers'] = array('menu_tree__submenu');
    // Prevent dropdown functions from being added to management menu so it
    // does not affect the navbar module.

    if (($element['#original_link']['menu_name'] == 'management') && (module_exists('navbar'))) {
      $sub_menu = drupal_render($element['#below']);
    }
    elseif ((!empty($element['#original_link']['depth'])) && ($element['#original_link']['depth'] == 1)) {
      // Add our own wrapper.
      unset($element['#below']['#theme_wrappers']);
      $sub_menu = '<ul class="dropdown-content" id="dropdown-menu">' . drupal_render($element['#below']) . '</ul>';
      // Generate as standard dropdown.
      $element['#title'] .= ' <i class="mdi-navigation-arrow-drop-down right"></i>';
      $element['#attributes']['class'][] = 'dropdown';
      $element['#localized_options']['html'] = TRUE;

      // Set dropdown trigger element to # to prevent inadvertant page loading
      // when a submenu link is clicked.
      $element['#localized_options']['attributes']['data-target'] = '#';
      $element['#localized_options']['attributes']['class'][] = 'dropdown-button';
      $element['#localized_options']['attributes']['data-activates'] = 'dropdown-menu';
    }
  }
  // On primary navigation menu, class 'active' is not set on active menu item.
  // @see https://drupal.org/node/1896674
  if (($element['#href'] == $_GET['q'] || ($element['#href'] == '<front>' && drupal_is_front_page())) && (empty($element['#localized_options']['language']))) {
    $element['#attributes']['class'][] = 'active';
  }
  $output = l($element['#title'], $element['#href'], $element['#localized_options']);
  return '<li' . drupal_attributes($element['#attributes']) . '>' . $output . $sub_menu . "</li>\n";
}
