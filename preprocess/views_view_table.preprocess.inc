<?php

/**
 * @file
 * Contains preprocess functions for table.
 */

/**
 * Implements hook_preprocess_views_view_table().
 */
function omega_materialize_preprocess_views_view_table(&$variables) {
  $variables['classes_array'][] = 'bordered';
  $variables['classes_array'][] = 'responsive-table';
  $variables['classes_array'][] = 'striped';
}