<?php

/**
 * Implements hook_preprocess_page().
 */
function omega_materialize_preprocess_page(&$variables) {
  // Dynamic sidebars.
  $sidebar_first = !empty($variables['page']['sidebar_first'])
    ? isset($variables['page']['sidebar_first']['#sorted']) : FALSE;
  $sidebar_second = !empty($variables['page']['sidebar_second'])
    ? isset($variables['page']['sidebar_second']['#sorted']) : FALSE;

  if ($sidebar_first && $sidebar_second) {
    $variables['main_css'] = 'col s12 m6';
    $variables['first_sidebar_css'] = 'col s12 m3';
    $variables['second_sidebar_css'] = 'col s12 m3';
  }
  elseif (!$sidebar_first && $sidebar_second) {
    $variables['main_css'] = 'col s12 m9';
    $variables['first_sidebar_css'] = '';
    $variables['second_sidebar_css'] = 'col s12 m3';
  }
  elseif ($sidebar_first && !$sidebar_second) {
    $variables['main_css'] = 'col s12 m9';
    $variables['first_sidebar_css'] = 'col s12 m3';
    $variables['second_sidebar_css'] = '';
  }
  else {
    $variables['main_css'] = '';
    $variables['first_sidebar_css'] = '';
    $variables['second_sidebar_css'] = '';
  }
}
