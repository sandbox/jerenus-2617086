<?php

/**
 * Implements hook_preprocess_html().
 */
function omega_materialize_preprocess_html(&$variables) {
  // Check the library module existing and materialize library existing.
  if (check_library('materialize', 'css/materialize.min.css') && check_library('materialize', 'js/materialize.min.js')) {
    drupal_add_css(libraries_get_path('materialize') . '/css/materialize.min.css', array(
      'group' => CSS_THEME,
      'every_page' => TRUE
    ));
    drupal_add_js(libraries_get_path('materialize') . '/js/materialize.min.js', array(
      'group' => JS_THEME,
      'every_page' => TRUE
    ));
  }
  else {
    drupal_set_message(t('Please download the latest version of <a href="http://materializecss.com/getting-started.html">Materialize</a> and place all content after extract to a new libraries folder <em>/materialize</em> within your drupal installation. %css_file and %js_file', array(
      '%css_file' => 'sites/all/libraries/materialize/css/materialize.min.css',
      '%js_file' => 'sites/all/libraries/materialize/js/materialize.min.js'
    )), 'error');
  }

  if (!module_exists('jquery_update')) {
    drupal_set_message(t('Materialize requires the <a href="https://www.drupal.org/project/jquery_update/">jQuery Update module</a> to be installed.'), 'error');
  }
  elseif (version_compare(variable_get('jquery_update_jquery_version'), '1.8', '<')) {
    drupal_set_message(t('Materialize requires at least jQuery 1.8. Please enable it by <a href="/admin/config/development/jquery_update/">clicking here</a>.'), 'error');
  }

  // Sidebar types
  $sidebar_first = isset($variables['page']['sidebar_first']['#sorted']);
  $sidebar_second = isset($variables['page']['sidebar_second']['#sorted']);

  if ($sidebar_first && $sidebar_second) {
    $variables['classes_array'][] = 'both-sidebar';
  }
  elseif ($sidebar_first) {
    $variables['classes_array'][] = 'first-sidebar';
  }
  elseif ($sidebar_second) {
    $variables['classes_array'][] = 'second-sidebar';
  }
}